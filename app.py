from curses.ascii import NUL
import sys
import falcon
import hashlib
import random
from datetime import datetime
from pymongo import MongoClient
import pymongo
import constants
from wsgiref.simple_server import make_server

sys.setrecursionlimit(1500000000)
print(sys.getrecursionlimit())


class ThingsResource:
    counter = 0
    client = None

    def __init__(self, client) -> None:
        self.client = client

    def on_get(self, req, resp):
        db = client[constants.MONGO_DB]
        print(db)
        print(client.server_info())
        collection_name = db["test_api"]
        init = datetime.now()
        random.seed(50)
        hash = self.od()
        end = datetime.now()
        dif = end - init
        print("END: {}".format(hash))
        data = {
            'init': init.strftime("%m/%d/%Y, %H:%M:%S"),
            'end': end.strftime("%m/%d/%Y, %H:%M:%S"),
            'dif': str(dif),
            'hash': hash
        }
        print(data)
        collection_name.insert_one(data)
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = falcon.MEDIA_TEXT  # Default is JSON, so override
        resp.text = ('\nTwo things awe me most, the starry sky '
                     'above me and the moral law within me.\n'
                     '\n'
                     '    ~ Immanuel Kant\n\n'
                     '{}\n'
                     '{}\n'
                     '{}'.format(hash, self.counter, dif))

    def sha256(self):
        self.counter += 1
        h = hashlib.new('sha256')
        s = "Nobody inspects the spammish repetition {}".format(random.random())
        h.update(bytes(s, 'utf-8'))
        return h.hexdigest()

    def ed(self):
        y = self.sha256()
        if y != '000':
            self.ed()

        return y

    def od(self):
        y = self.sha256()
        while y[:5] != '00000':
            y = self.sha256()

        return y


# falcon.App instances are callable WSGI apps
# in larger applications the app is created in a separate file
app = falcon.App()
client = MongoClient(
    host=constants.MONGO_HOST,
    username=constants.MONGO_USER,
    password=constants.MONGO_PWD
)

# Resources are represented by long-lived class instances
things = ThingsResource(client)

# things will handle all requests to the '/things' URL path
app.add_route('/things', things)

if __name__ == '__main__':
    with make_server('', 8000, app) as httpd:
        print('Serving on port 8000...')

        # Serve until process is killed
        httpd.serve_forever()
